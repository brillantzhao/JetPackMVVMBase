package com.zzk.jpmvvmbase.callback.livedata

import androidx.lifecycle.MutableLiveData

/**
 * 自定义的Boolean类型 MutableLiveData 提供了默认值，避免取值的时候还要判空
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 14:14
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 14:14
 * @UpdateRemark:
 * @Version: 1.0.0
 */
class BooleanLiveData : MutableLiveData<Boolean>() {

    override fun getValue(): Boolean {
        return super.getValue() ?: false
    }
}

