package com.zzk.jpmvvmbase.base

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import com.zzk.jpmvvmbase.callback.livedata.event.EventLiveData

/**
 *  ViewModel的基类 使用ViewModel类，放弃AndroidViewModel，原因：用处不大 完全有其他方式获取Application上下文
 *
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 14:14
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 14:14
 * @UpdateRemark:
 * @Version: 1.0.0
 */
open class BaseViewModel : ViewModel(), LifecycleObserver {

    //##########################  custom variables start ##########################################

    val loadingChange: UiLoadingChange by lazy { UiLoadingChange() }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    /**
     * 内置封装好的可通知Activity/fragment 显示隐藏加载框 因为需要跟网络请求显示隐藏loading配套才加的，不然我加他个鸡儿加
     */
    inner class UiLoadingChange {
        //显示加载框
        val showDialog by lazy { EventLiveData<String>() }

        //隐藏
        val dismissDialog by lazy { EventLiveData<Boolean>() }
    }

    //##########################  custom metohds end   ############################################

}