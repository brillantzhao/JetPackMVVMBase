package com.zzk.jpmvvmbase.base

import android.os.Bundle

/**
 *
 * @ProjectName:    JetPackMVVMBase
 * @Package:        com.zzk.jpmvvmbase.base
 * @ClassName:      IBaseView
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.21 9:57
 * @UpdateUser:
 * @UpdateDate:     2021.1.21 9:57
 * @UpdateRemark:
 * @Version:        1.0.0
 */
interface IBaseView {

    /**
     * 初始化界面传递参数
     */
    fun initParam()

    /**
     * 初始化数据
     */
    fun initData(savedInstanceState: Bundle?)

    /**
     * 初始化界面观察者的监听
     */
    fun initViewObservable()

}