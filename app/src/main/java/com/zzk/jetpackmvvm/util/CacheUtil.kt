package com.zzk.jetpackmvvm.util

import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.tencent.mmkv.MMKV
import com.zzk.jetpackmvvm.data.model.bean.UserInfo

/**
 *
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 15:50
 * @UpdateRemark:
 * @Version: 1.0.0
 */
object CacheUtil {

    /**
     * 获取保存的账户信息
     */
    fun getUser(): UserInfo? {
        val mmkv = MMKV.mmkvWithID("app")
        val userStr = mmkv?.decodeString("user")
        return if (TextUtils.isEmpty(userStr)) {
            null
        } else {
            Gson().fromJson(userStr, UserInfo::class.java)
        }
    }

    /**
     * 设置账户信息
     */
    fun setUser(userResponse: UserInfo?) {
        val mmkv = MMKV.mmkvWithID("app")
        if (userResponse == null) {
            mmkv?.encode("user", "")
            setIsLogin(false)
        } else {
            mmkv?.encode("user", Gson().toJson(userResponse))
            setIsLogin(true)
        }

    }

    /**
     * 是否已经登录
     */
    fun isLogin(): Boolean? {
        val mmkv = MMKV.mmkvWithID("app")
        return mmkv?.decodeBool("login", false)
    }

    /**
     * 设置是否已经登录
     */
    fun setIsLogin(isLogin: Boolean) {
        val mmkv = MMKV.mmkvWithID("app")
        mmkv?.encode("login", isLogin)
    }

    /**
     * 是否是第一次登陆
     */
    fun isFirst(): Boolean? {
        val mmkv = MMKV.mmkvWithID("app")
        return mmkv?.decodeBool("first", true)
    }

    /**
     * 是否是第一次登陆
     */
    fun setFirst(first: Boolean): Boolean? {
        val mmkv = MMKV.mmkvWithID("app")
        return mmkv?.encode("first", first)
    }

    /**
     * 首页是否开启获取指定文章
     */
    fun isNeedTop(): Boolean? {
        val mmkv = MMKV.mmkvWithID("app")
        return mmkv?.decodeBool("top", true)
    }

    /**
     * 设置首页是否开启获取指定文章
     */
    fun setIsNeedTop(isNeedTop: Boolean): Boolean? {
        val mmkv = MMKV.mmkvWithID("app")
        return mmkv?.encode("top", isNeedTop)
    }

    /**
     * 获取搜索历史缓存数据
     */
    fun getSearchHistoryData(): ArrayList<String> {
        val mmkv = MMKV.mmkvWithID("cache")
        val searchCacheStr = mmkv?.decodeString("history")
        if (!TextUtils.isEmpty(searchCacheStr)) {
            return Gson().fromJson(searchCacheStr, object : TypeToken<ArrayList<String>>() {}.type)
        }
        return arrayListOf()
    }

    fun setSearchHistoryData(searchResponseStr: String) {
        val mmkv = MMKV.mmkvWithID("cache")
        mmkv?.encode("history", searchResponseStr)
    }
}