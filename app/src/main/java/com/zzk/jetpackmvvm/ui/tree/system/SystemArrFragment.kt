package com.zzk.jetpackmvvm.ui.tree.system

import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.base.JPBaseFragment

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.zzk.jpmvvmbase.ext.navController
import com.zzk.jetpackmvvm.data.model.bean.SystemResponse
import com.zzk.jetpackmvvm.databinding.FragmentSystemBinding
import com.zzk.jetpackmvvm.ext.bindViewPager2
import com.zzk.jetpackmvvm.ext.init
import com.zzk.jetpackmvvm.ext.initClose
import com.zzk.jetpackmvvm.ui.tree.system.child.SystemChildFragment
import com.zzk.jpmvvmbase.base.BaseViewModel
import kotlinx.android.synthetic.main.include_toolbar.*
import kotlinx.android.synthetic.main.include_viewpager.*

/**
 * 作者　: hegaojian
 * 时间　: 2020/3/4
 * 描述　:
 */
class SystemArrFragment : JPBaseFragment<BaseViewModel, FragmentSystemBinding>() {

    lateinit var data: SystemResponse

    var index = 0

    private var fragments: ArrayList<Fragment> = arrayListOf()

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_system
    }

    override fun initData(savedInstanceState: Bundle?) {
        arguments?.let {
            data = it.getParcelable("data")!!
            index = it.getInt("index")
        }
        toolbar.initClose(data.name) {
            navController().navigateUp()
        }
        //初始化时设置顶部主题颜色
        appViewModel.appColor.value?.let { viewpager_linear.setBackgroundColor(it) }
        //设置栏目标题居左显示
        (magic_indicator.layoutParams as FrameLayout.LayoutParams).gravity = Gravity.LEFT
    }

    override fun lazyLoadData() {
        data.children.forEach {
            fragments.add(SystemChildFragment.newInstance(it.id))
        }
        //初始化viewpager2
        view_pager.init(this, fragments)
        //初始化 magic_indicator
        magic_indicator.bindViewPager2(view_pager, data.children.map { it.name })

        view_pager.offscreenPageLimit = fragments.size

        view_pager.postDelayed({
            view_pager.currentItem = index
        }, 100)

    }

}