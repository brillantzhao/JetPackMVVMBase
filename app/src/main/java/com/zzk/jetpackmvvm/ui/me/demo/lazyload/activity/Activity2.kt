package com.zzk.jetpackmvvm.ui.me.demo.lazyload.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.adapter.FragmentLazyStatePageAdapter
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generate123FragmentTitles
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generate123Fragments

/**
 * Author:  andy.xwt
 * Date:    2020-01-14 14:48
 * Description:
 */
class Activity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1)
        initView()
    }

    private fun initView() {
        val viewPager = findViewById<ViewPager>(R.id.view_pager).apply {
            adapter = FragmentLazyStatePageAdapter(
                supportFragmentManager,
                generate123Fragments().values.toMutableList(),
                generate123FragmentTitles()
            )
        }

        findViewById<TabLayout>(R.id.tab_layout).setupWithViewPager(viewPager)
    }

}