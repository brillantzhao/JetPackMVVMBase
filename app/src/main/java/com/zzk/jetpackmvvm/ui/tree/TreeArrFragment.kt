package com.zzk.jetpackmvvm.ui.tree

import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.base.JPBaseFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.zzk.jpmvvmbase.ext.navController
import com.zzk.jpmvvmbase.ext.navigateAction
import com.zzk.jetpackmvvm.databinding.FragmentViewpagerBinding
import com.zzk.jetpackmvvm.ext.*
import com.zzk.jetpackmvvm.ui.tree.ask.AskFragment
import com.zzk.jetpackmvvm.ui.tree.navigation.NavigationFragment
import com.zzk.jetpackmvvm.ui.tree.plaza.PlazaFragment
import com.zzk.jetpackmvvm.ui.tree.system.SystemFragment
import com.zzk.jetpackmvvm.util.CacheUtil
import com.zzk.jpmvvmbase.base.BaseViewModel
import kotlinx.android.synthetic.main.include_viewpager.*

/**
 * 广场模块父Fragment管理四个子fragment
 * @ProjectName:
 * @Package:        com.zzk.jetpackmvvm
 * @ClassName:
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 15:50
 * @UpdateRemark:
 * @Version:        1.0.0
 */
class TreeArrFragment : JPBaseFragment<BaseViewModel, FragmentViewpagerBinding>() {

    //##########################  custom variables start ##########################################

    var titleData = arrayListOf("广场", "每日一问", "体系", "导航")

    private var fragments: ArrayList<Fragment> = arrayListOf()

    init {
        fragments.add(PlazaFragment())
        fragments.add(AskFragment())
        fragments.add(SystemFragment())
        fragments.add(NavigationFragment())
    }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_viewpager
    }

    override fun initData(savedInstanceState: Bundle?) {
        //初始化时设置顶部主题颜色
        appViewModel.appColor.value?.let { setUiTheme(it, viewpager_linear) }
        include_viewpager_toolbar.run {
            inflateMenu(R.menu.todo_menu)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.todo_add -> {
                        if (CacheUtil.isLogin() == true) {
                            navController().navigateAction(R.id.action_mainfragment_to_addAriticleFragment)
                        } else {
                            navController().navigateAction(R.id.action_to_loginFragment)
                        }
                    }
                }
                true
            }
        }
    }

    override fun lazyLoadData() {
        //初始化viewpager2
        view_pager.init(this, fragments).offscreenPageLimit = fragments.size
        //初始化 magic_indicator
        magic_indicator.bindViewPager2(view_pager, mStringList = titleData) {
            if (it != 0) {
                include_viewpager_toolbar.menu.clear()
            } else {
                include_viewpager_toolbar.menu.hasVisibleItems().let { flag ->
                    if (!flag) include_viewpager_toolbar.inflateMenu(R.menu.todo_menu)
                }
            }
        }
    }

    override fun initViewObservable() {
        appViewModel.appColor.observe(viewLifecycleOwner, Observer {
            setUiTheme(it, viewpager_linear)
        })
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}