package com.zzk.jetpackmvvm.ui.error

import android.content.ClipData
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import cat.ereza.customactivityoncrash.CustomActivityOnCrash
import com.blankj.utilcode.util.ToastUtils
import com.zzk.jetpackmvvm.R
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.base.JPBaseActivity
import com.zzk.jetpackmvvm.databinding.ActivityErrorBinding
import com.zzk.jpmvvmbase.ext.util.clipboardManager
import com.zzk.jpmvvmbase.ext.view.clickNoRepeat
import com.zzk.jetpackmvvm.ext.init
import com.zzk.jetpackmvvm.ext.showMessage
import com.zzk.jetpackmvvm.util.SettingUtil
import com.zzk.jetpackmvvm.util.StatusBarUtil
import kotlinx.android.synthetic.main.activity_error.*
import kotlinx.android.synthetic.main.include_toolbar.*

/**
 *
 * @ProjectName:    JetPackMVVMBase
 * @Package:        com.zzk.jpmvvmbase.ui.error
 * @ClassName:      ErrorActivity
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 16:00
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 16:00
 * @UpdateRemark:
 * @Version:        1.0.0
 */
class ErrorActivity : JPBaseActivity<BaseViewModel, ActivityErrorBinding>() {

    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(savedInstanceState: Bundle?): Int {
        return R.layout.activity_error
    }

    override fun initData(savedInstanceState: Bundle?) {
        toolbar.init("发生错误")
        supportActionBar?.setBackgroundDrawable(ColorDrawable(SettingUtil.getColor(this)))
        StatusBarUtil.setColor(this, SettingUtil.getColor(this), 0)
        val config = CustomActivityOnCrash.getConfigFromIntent(intent)
        errorRestart.clickNoRepeat {
            config?.run {
                CustomActivityOnCrash.restartApplication(this@ErrorActivity, this)
            }
        }
        errorSendError.clickNoRepeat {
            CustomActivityOnCrash.getStackTraceFromIntent(intent)?.let {
                showMessage(it, "发现有Bug", "复制日志", {
                    val mClipData = ClipData.newPlainText("errorLog", it)
                    // 将ClipData内容放到系统剪贴板里。
                    clipboardManager?.primaryClip = mClipData
                    ToastUtils.showShort("已复制错误日志")
                }, "关闭")
            }
        }
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}