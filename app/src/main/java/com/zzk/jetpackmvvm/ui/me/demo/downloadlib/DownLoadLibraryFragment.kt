package com.zzk.jetpackmvvm.ui.me.demo.downloadlib

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.blankj.utilcode.util.LogUtils
import com.liulishuo.filedownloader.FileDownloader
import kotlinx.android.synthetic.main.fragment_download_library.*
import kotlinx.android.synthetic.main.include_toolbar.*
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.base.JPBaseFragment
import com.zzk.jpmvvmbase.base.appContext
import com.zzk.jetpackmvvm.databinding.FragmentDownloadLibraryBinding
import com.zzk.jetpackmvvm.ext.*
import com.zzk.jpmvvmbase.ext.download.DownloadResultState
import com.zzk.jpmvvmbase.ext.download.FileTool
import com.zzk.jpmvvmbase.ext.download.FileTool.getBasePath
import com.zzk.jpmvvmbase.ext.navController

/**集成了GitHub 高star的一个下载库 https://github.com/lingochamp/FileDownloader，供大家参考
 * @author : hgj
 * @date   : 2020/7/13
 */
class DownLoadLibraryFragment :
    JPBaseFragment<DownloadLibraryViewModel, FragmentDownloadLibraryBinding>() {

    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_download_library
    }

    override fun initData(savedInstanceState: Bundle?) {
        mDatabind.click = ProxyClick()
        //第三方下载库注册， 可以直接放在application里面注册
        FileDownloader.setup(appContext)
        toolbar.initClose("三方库下载") {
            navController().navigateUp()
        }
    }

    override fun initViewObservable() {
        mViewModel.downloadData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is DownloadResultState.Pending -> {
                    //开始下载
                    LogUtils.d("开始下载")
                }
                is DownloadResultState.Progress -> {
                    //下载中
                    downloadLibraryProgressBar.progress = it.progress
                    LogUtils.d("下载中 ${it.soFarBytes}/${it.totalBytes}")
                    downloadLibraryProgress.text = "${it.progress}%"
                    downloadLibrarySize.text =
                        "${FileTool.bytes2kb(it.soFarBytes)}/${FileTool.bytes2kb(it.totalBytes)}"
                }
                is DownloadResultState.Success -> {
                    //下载成功
                    downloadLibraryProgressBar.progress = 100
                    downloadLibraryProgress.text = "100%"
                    downloadLibrarySize.text =
                        "${FileTool.bytes2kb(it.totalBytes)}/${FileTool.bytes2kb(it.totalBytes)}"
                    showMessage("下载成功--文件地址：${it.filePath}")
                }
                is DownloadResultState.Pause -> {
                    showMessage("下载暂停")
                }
                is DownloadResultState.Error -> {
                    //下载失败
                    showMessage("错误信息:${it.errorMsg}")
                }
            }
        })
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    inner class ProxyClick {
        fun downloadLibrary() {
            //测试一下利用三方库下载
            mViewModel.downloadApkByLibrary(
                getBasePath(),
                "https://down.qq.com/qqweb/QQlite/Android_apk/qqlite_4.0.1.1060_537064364.apk",
                "qq"
            )
        }

        fun cancel() {
            mViewModel.downloadCancel()
        }

        fun pause() {
            mViewModel.downloadPause()
        }
    }

    //##########################  custom metohds end   ############################################

}