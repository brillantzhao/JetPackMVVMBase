package com.zzk.jetpackmvvm.ui.main.search

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.ext.setAdapterAnimation
import com.zzk.jetpackmvvm.util.SettingUtil

class SearcHistoryAdapter(data: MutableList<String>) :
    BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_history, data) {

    init {
        SettingUtil.getListMode()?.let { setAdapterAnimation(it) }
    }

    override fun convert(holder: BaseViewHolder, item: String) {
        holder.setText(R.id.item_history_text, item)
    }

}