package com.zzk.jetpackmvvm.ui.me.demo.lazyload.activity

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.FRAGMENT_ONE
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.FRAGMENT_THREE
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.FRAGMENT_TWO
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generate123Fragments
import com.zzk.jpmvvmbase.ext.loadFragments
import com.zzk.jpmvvmbase.ext.showHideFragment

/**
 * Author:  andy.xwt
 * Date:    2020-01-14 14:48
 * Description:
 */
class Activity3 : AppCompatActivity(), View.OnClickListener {

    private lateinit var fragmentsMap: Map<String, Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_3)
        initView()
    }

    private fun initView() {
        findViewById<Button>(R.id.btn_1).setOnClickListener(this)
        findViewById<Button>(R.id.btn_2).setOnClickListener(this)
        findViewById<Button>(R.id.btn_3).setOnClickListener(this)
        fragmentsMap = generate123Fragments()
        loadFragments(R.id.fl_container, 0, *fragmentsMap.values.toTypedArray())
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_1 -> {
                showHideFragment(fragmentsMap.getValue(FRAGMENT_ONE))
            }
            R.id.btn_2 -> {
                showHideFragment(fragmentsMap.getValue(FRAGMENT_TWO))
            }
            R.id.btn_3 -> {
                showHideFragment(fragmentsMap.getValue(FRAGMENT_THREE))
            }
        }
    }
}