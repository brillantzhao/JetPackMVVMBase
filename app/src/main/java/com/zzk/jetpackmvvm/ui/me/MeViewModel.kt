package com.zzk.jetpackmvvm.ui.me

import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jpmvvmbase.callback.databind.IntObservableField
import com.zzk.jpmvvmbase.callback.databind.StringObservableField
import com.zzk.jpmvvmbase.callback.livedata.UnPeekLiveData
import com.zzk.jetpackmvvm.util.ColorUtil

/**
 *
 * @ProjectName:
 * @Package:        com.zzk.jetpackmvvm
 * @ClassName:
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 15:50
 * @UpdateRemark:
 * @Version:        1.0.0
 */
class MeViewModel : BaseViewModel() {

    var name = StringObservableField("请先登录~")

    var integral = IntObservableField(0)

    var info = StringObservableField("id：--　排名：-")

    var imageUrl = StringObservableField(ColorUtil.randomImage())

    var testString = UnPeekLiveData<String>()

}