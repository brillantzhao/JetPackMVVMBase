package com.zzk.jetpackmvvm.ui.splash

import android.view.View
import android.widget.TextView
import com.zhpan.bannerview.BaseViewHolder
import com.zzk.jetpackmvvm.R

/**
 *
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 15:50
 * @UpdateRemark:
 * @Version: 1.0.0
 */
class SplashBannerViewHolder(view: View) : BaseViewHolder<String>(view) {

    override fun bindData(data: String?, position: Int, pageSize: Int) {
        val textView = findView<TextView>(R.id.banner_text)
        textView.text = data
    }

}
