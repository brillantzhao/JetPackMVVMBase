package com.zzk.jetpackmvvm.ui.me.demo.downLoad

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.blankj.utilcode.util.LogUtils
import kotlinx.android.synthetic.main.fragment_download.*
import kotlinx.android.synthetic.main.include_toolbar.*
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.base.JPBaseFragment
import com.zzk.jetpackmvvm.databinding.FragmentDownloadBinding
import com.zzk.jetpackmvvm.ext.*
import com.zzk.jpmvvmbase.ext.download.DownloadResultState
import com.zzk.jpmvvmbase.ext.download.FileTool
import com.zzk.jpmvvmbase.ext.download.FileTool.getBasePath
import com.zzk.jpmvvmbase.ext.navController

/**
 * 文件下载 示例 框架自带的，功能比较简单，肯定木得三方库那么强大
 * @ProjectName:
 * @Package:        com.zzk.jetpackmvvm
 * @ClassName:
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 15:50
 * @UpdateRemark:
 * @Version:        1.0.0
 */
class DownLoadFragment : JPBaseFragment<DownloadViewModel, FragmentDownloadBinding>() {

    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_download
    }

    override fun initData(savedInstanceState: Bundle?) {
        mDatabind.click = ProxyClick()
        toolbar.initClose("框架自带普通下载") {
            navController().navigateUp()
        }
    }

    override fun initViewObservable() {
        mViewModel.downloadData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is DownloadResultState.Pending -> {
                    //开始下载
                    LogUtils.d("开始下载")
                }
                is DownloadResultState.Progress -> {
                    //下载中
                    downloadProgressBar.progress = it.progress
                    LogUtils.d("下载中 ${it.soFarBytes}/${it.totalBytes}")
                    downloadProgress.text = "${it.progress}%"
                    downloadSize.text =
                        "${FileTool.bytes2kb(it.soFarBytes)}/${FileTool.bytes2kb(it.totalBytes)}"
                }
                is DownloadResultState.Success -> {
                    //下载成功
                    downloadProgressBar.progress = 100
                    downloadProgress.text = "100%"
                    downloadSize.text =
                        "${FileTool.bytes2kb(it.totalBytes)}/${FileTool.bytes2kb(it.totalBytes)}"
                    showMessage("下载成功--文件地址：${it.filePath}")
                }
                is DownloadResultState.Pause -> {
                    showMessage("下载暂停")
                }
                is DownloadResultState.Error -> {
                    //下载失败
                    showMessage("错误信息:${it.errorMsg}")
                }
            }
        })
    }

    //##########################  override custom metohds start ###################################

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    inner class ProxyClick {
        fun download() {
            //普通下载
            mViewModel.downloadApk(
                getBasePath(),
                "https://down.qq.com/qqweb/QQlite/Android_apk/qqlite_4.0.1.1060_537064364.apk",
                "qq"
            )
        }

        fun cancel() {
            mViewModel.downloadCancel("qq")
        }

        fun pause() {
            mViewModel.downloadPause("qq")
        }
    }

    //##########################  custom metohds end   ############################################

}