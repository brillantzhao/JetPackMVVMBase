package com.zzk.jetpackmvvm.ui.tree.navigation

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.data.model.bean.AriticleResponse
import com.zzk.jetpackmvvm.ext.setAdapterAnimation
import com.zzk.jpmvvmbase.ext.util.toHtml
import com.zzk.jetpackmvvm.util.ColorUtil
import com.zzk.jetpackmvvm.util.SettingUtil

class NavigationChildAdapter(data: ArrayList<AriticleResponse>) :
    BaseQuickAdapter<AriticleResponse, BaseViewHolder>(R.layout.flow_layout, data) {

    init {
        SettingUtil.getListMode()?.let { setAdapterAnimation(it) }
    }

    override fun convert(holder: BaseViewHolder, item: AriticleResponse) {
        holder.setText(R.id.flow_tag, item.title.toHtml())
        holder.setTextColor(R.id.flow_tag, ColorUtil.randomColor())
    }

}