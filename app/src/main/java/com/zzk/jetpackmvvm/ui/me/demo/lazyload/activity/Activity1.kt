package com.zzk.jetpackmvvm.ui.me.demo.lazyload.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.base.JPBaseActivity
import com.zzk.jetpackmvvm.databinding.Activity1Binding
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.adapter.FragmentLazyPagerAdapter
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generate123FragmentTitles
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generate123Fragments
import com.zzk.jpmvvmbase.base.BaseViewModel

/**
 * Author:  andy.xwt
 * Date:    2020-01-14 14:48
 * Description:
 */
class Activity1 : JPBaseActivity<BaseViewModel, Activity1Binding>() {

    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(savedInstanceState: Bundle?): Int {
        return R.layout.activity_1
    }

    override fun initData(savedInstanceState: Bundle?) {
        val viewPager = findViewById<ViewPager>(R.id.view_pager).apply {
            adapter = FragmentLazyPagerAdapter(
                supportFragmentManager,
                generate123Fragments().values.toMutableList(),
                generate123FragmentTitles()
            )
        }
        findViewById<TabLayout>(R.id.tab_layout).setupWithViewPager(viewPager)
    }

    override fun initViewObservable() {
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}