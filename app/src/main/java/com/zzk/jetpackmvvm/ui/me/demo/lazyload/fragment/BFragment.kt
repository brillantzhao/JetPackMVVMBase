package com.zzk.jetpackmvvm.ui.me.demo.lazyload.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.blankj.utilcode.util.LogUtils
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.databinding.FragmentBBinding
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.FRAGMENT_ONE
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.FRAGMENT_THREE
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.FRAGMENT_TWO
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generate123Fragments
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jpmvvmbase.base.LazyBaseFragment
import com.zzk.jpmvvmbase.ext.loadFragments
import com.zzk.jpmvvmbase.ext.showHideFragment

/**
 * Author:  andy.xwt
 * Date:    2020-01-14 18:34
 * Description:
 */
class BFragment : LazyBaseFragment<BaseViewModel, FragmentBBinding>(), View.OnClickListener {

    //##########################  custom variables start ##########################################

    private lateinit var fragmentsMap: Map<String, Fragment>

    companion object {
        fun newInstance() = BFragment()
    }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_b
    }

    override fun initData(savedInstanceState: Bundle?) {
        super.initData(savedInstanceState)
        LogUtils.i("BFragment->initData")
    }

    override fun lazyLoadData() {
        LogUtils.i("BFragment->lazyLoadData")
        view?.findViewById<Button>(R.id.btn_1)?.setOnClickListener(this)
        view?.findViewById<Button>(R.id.btn_2)?.setOnClickListener(this)
        view?.findViewById<Button>(R.id.btn_3)?.setOnClickListener(this)
        fragmentsMap = generate123Fragments()
        loadFragments(R.id.fl_b_container, 0, *fragmentsMap.values.toTypedArray())
    }

    override fun showLoadingDialog(message: String) {
    }

    override fun dismissLoadingDialog() {
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_1 -> {
                showHideFragment(fragmentsMap.getValue(FRAGMENT_ONE))
            }
            R.id.btn_2 -> {
                showHideFragment(fragmentsMap.getValue(FRAGMENT_TWO))
            }
            R.id.btn_3 -> {
                showHideFragment(fragmentsMap.getValue(FRAGMENT_THREE))
            }
        }
    }

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}
