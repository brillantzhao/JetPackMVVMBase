package com.zzk.jetpackmvvm.ui.me.demo

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.zzk.jpmvvmbase.ext.navController
import com.zzk.jpmvvmbase.ext.navigateAction
import kotlinx.android.synthetic.main.include_toolbar.*
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.base.JPBaseFragment
import com.zzk.jetpackmvvm.databinding.FragmentDemoBinding
import com.zzk.jetpackmvvm.ext.*
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.LazyLoadDemoActivity
import com.zzk.jpmvvmbase.base.BaseViewModel

/**
 * 放一些示例，目前只有 文件下载示例 后面想到什么加什么，作者那个比很懒，佛性添加
 * @ProjectName:
 * @Package:        com.zzk.jetpackmvvm
 * @ClassName:
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 15:50
 * @UpdateRemark:
 * @Version:        1.0.0
 */
class DemoFragment : JPBaseFragment<BaseViewModel, FragmentDemoBinding>() {

    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_demo
    }

    override fun initData(savedInstanceState: Bundle?) {
        mDatabind.click = ProxyClick()

        toolbar.initClose("示例") {
            navController().navigateUp()
        }
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    inner class ProxyClick {
        fun download() {
            //测试一下 普通的下载
            navController().navigateAction(R.id.action_demoFragment_to_downLoadFragment)
        }

        fun downloadLibrary() {
            //测试一下利用三方库下载
            navController().navigateAction(R.id.action_demoFragment_to_downLoadLibraryFragment)
        }

        fun fragmentLazyLoad() {
            //各种场景下的Fragment懒加载
            startActivity(LazyLoadDemoActivity::class.java)
        }
    }

    //##########################  custom metohds end   ############################################

}