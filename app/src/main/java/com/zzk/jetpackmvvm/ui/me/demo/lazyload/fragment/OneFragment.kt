package com.zzk.jetpackmvvm.ui.me.demo.lazyload.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.blankj.utilcode.util.LogUtils
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.databinding.FragmentTextBinding
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.ARG_STR
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jpmvvmbase.base.LazyBaseFragment

/**
 * Author:  andy.xwt
 * Date:    2020-01-14 18:33
 * Description:
 */
class OneFragment : LazyBaseFragment<BaseViewModel, FragmentTextBinding>() {

    //##########################  custom variables start ##########################################

    companion object {
        fun newInstance(text: String) = OneFragment().apply {
            arguments = Bundle().apply { putString(ARG_STR, text) }
        }
    }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_text
    }

    override fun initData(savedInstanceState: Bundle?) {
        super.initData(savedInstanceState)
        LogUtils.i("OneFragment->initData")
    }

    override fun lazyLoadData() {
        LogUtils.i("OneFragment->lazyLoadData")
        arguments?.takeIf { it.containsKey(ARG_STR) }?.apply {
            view?.findViewById<TextView>(R.id.tv_text)?.text = getString(ARG_STR)
        }
    }

    override fun showLoadingDialog(message: String) {
    }

    override fun dismissLoadingDialog() {
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################
}