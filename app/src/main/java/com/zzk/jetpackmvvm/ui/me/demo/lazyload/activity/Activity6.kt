package com.zzk.jetpackmvvm.ui.me.demo.lazyload.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.adapter.FragmentLazyPagerAdapter
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generateABCFragmentTitles
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generateABCFragments

class Activity6 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1)
        initView()
    }

    private fun initView() {
        val viewPager = findViewById<ViewPager>(R.id.view_pager).apply {
            adapter = FragmentLazyPagerAdapter(
                supportFragmentManager,
                generateABCFragments().values.toMutableList(),
                generateABCFragmentTitles()
            )
        }
        findViewById<TabLayout>(R.id.tab_layout).setupWithViewPager(viewPager)
    }

}