package com.zzk.jetpackmvvm.ui.me.collect

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.zzk.jpmvvmbase.ext.navController
import kotlinx.android.synthetic.main.fragment_collect.*
import kotlinx.android.synthetic.main.include_toolbar.*
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.base.JPBaseFragment
import com.zzk.jetpackmvvm.databinding.FragmentCollectBinding
import com.zzk.jetpackmvvm.ext.*
import com.zzk.jetpackmvvm.requestviewmodel.RequestCollectViewModel
import com.zzk.jetpackmvvm.ui.me.collect.article.CollectAriticleFragment
import com.zzk.jetpackmvvm.ui.me.collect.url.CollectUrlFragment

/**
 *
 * @ProjectName:
 * @Package:        com.zzk.jetpackmvvm
 * @ClassName:
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 15:50
 * @UpdateRemark:
 * @Version:        1.0.0
 */
class CollectFragment : JPBaseFragment<RequestCollectViewModel, FragmentCollectBinding>() {

    //##########################  custom variables start ##########################################

    var titleData = arrayListOf("文章", "网址")

    private var fragments: ArrayList<Fragment> = arrayListOf()

    init {
        fragments.add(CollectAriticleFragment())
        fragments.add(CollectUrlFragment())
    }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_collect
    }

    override fun initData(savedInstanceState: Bundle?) {
        //初始化时设置顶部主题颜色
        appViewModel.appColor.value?.let { collect_viewpager_linear.setBackgroundColor(it) }
        //初始化viewpager2
        collect_view_pager.init(this, fragments)
        //初始化 magic_indicator
        collect_magic_indicator.bindViewPager2(collect_view_pager, mStringList = titleData)
        toolbar.initClose() {
            navController().navigateUp()
        }
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}