package com.zzk.jetpackmvvm.ui.me.integral

import androidx.databinding.ObservableField
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.data.model.bean.IntegralResponse

/**
 * 作者　: hegaojian
 * 时间　: 2020/3/10
 * 描述　:
 */
class IntegralViewModel : BaseViewModel() {

    var rank = ObservableField<IntegralResponse>()
}