package com.zzk.jetpackmvvm.ui.main

import android.view.View
import android.view.ViewGroup
import com.zhpan.bannerview.BaseBannerAdapter
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.data.model.bean.BannerResponse

/**
 *
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 15:50
 * @UpdateRemark:
 * @Version: 1.0.0
 */
class MainBannerAdapter : BaseBannerAdapter<BannerResponse, MainBannerViewHolder>() {

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.banner_itemmain
    }

    override fun createViewHolder(
        parent: ViewGroup,
        itemView: View,
        viewType: Int
    ): MainBannerViewHolder {
        return MainBannerViewHolder(itemView)
    }

    override fun onBind(
        holder: MainBannerViewHolder?,
        data: BannerResponse?,
        position: Int,
        pageSize: Int
    ) {
        holder?.bindData(data, position, pageSize);
    }

}
