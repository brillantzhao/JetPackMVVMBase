package com.zzk.jetpackmvvm.ui.me.demo.lazyload.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blankj.utilcode.util.LogUtils
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.databinding.FragmentABinding
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jpmvvmbase.base.LazyBaseFragment

/**
 * Author:  andy.xwt
 * Date:    2020-01-14 18:33
 * Description:
 */
class AFragment : LazyBaseFragment<BaseViewModel, FragmentABinding>() {

    //##########################  custom variables start ##########################################

    companion object {
        fun newInstance() = AFragment()
    }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_a
    }

    override fun initData(savedInstanceState: Bundle?) {
        super.initData(savedInstanceState)
        LogUtils.i("AFragment->initData")
    }

    override fun lazyLoadData() {
        LogUtils.i("AFragment->lazyLoadData")
    }

    override fun showLoadingDialog(message: String) {
    }

    override fun dismissLoadingDialog() {
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}
