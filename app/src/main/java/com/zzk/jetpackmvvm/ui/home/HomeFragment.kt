package com.zzk.jetpackmvvm.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.zzk.jetpackmvvm.R
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.base.JPBaseFragment
import com.zzk.jetpackmvvm.databinding.FragmentMainBinding
import com.zzk.jetpackmvvm.ext.init
import com.zzk.jetpackmvvm.ext.interceptLongClick
import com.zzk.jetpackmvvm.ext.setUiTheme
import com.zzk.jetpackmvvm.ui.main.MainFragment
import com.zzk.jetpackmvvm.ui.me.MeFragment
import com.zzk.jetpackmvvm.ui.project.ProjectFragment
import com.zzk.jetpackmvvm.ui.publicNumber.PublicNumberFragment
import com.zzk.jetpackmvvm.ui.tree.TreeArrFragment
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_main.*

class HomeFragment : JPBaseFragment<BaseViewModel, FragmentMainBinding>() {

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_home
    }

    override fun initData(savedInstanceState: Bundle?) {
        //初始化viewpager2
        mainViewpager.initMain(this)
        //初始化 bottomBar
        mainBottom.init {
            when (it) {
                R.id.menu_main -> mainViewpager.setCurrentItem(0, false)
                R.id.menu_project -> mainViewpager.setCurrentItem(1, false)
                R.id.menu_system -> mainViewpager.setCurrentItem(2, false)
                R.id.menu_public -> mainViewpager.setCurrentItem(3, false)
                R.id.menu_me -> mainViewpager.setCurrentItem(4, false)
            }
        }
        mainBottom.interceptLongClick(
            R.id.menu_main,
            R.id.menu_project,
            R.id.menu_system,
            R.id.menu_public,
            R.id.menu_me
        )
    }

    override fun initViewObservable() {
        appViewModel.appColor.observe(viewLifecycleOwner, Observer {
            setUiTheme(it, mainBottom)
        })
    }

    private fun ViewPager2.initMain(fragment: Fragment): ViewPager2 {
        //是否可滑动
        this.isUserInputEnabled = false
        this.offscreenPageLimit = 5
        //设置适配器
        adapter = object : FragmentStateAdapter(fragment) {
            override fun createFragment(position: Int): Fragment {
                when (position) {
                    0 -> {
                        return MainFragment()
                    }
                    1 -> {
                        return ProjectFragment()
                    }
                    2 -> {
                        return TreeArrFragment()
                    }
                    3 -> {
                        return PublicNumberFragment()
                    }
                    4 -> {
                        return MeFragment()
                    }
                    else -> {
                        return MainFragment()
                    }
                }
            }

            override fun getItemCount() = 5
        }
        return this
    }

}