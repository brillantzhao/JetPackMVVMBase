package com.zzk.jetpackmvvm.ui.splash

import android.view.View
import android.view.ViewGroup
import com.zhpan.bannerview.BaseBannerAdapter
import com.zzk.jetpackmvvm.R

/**
 *
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 15:50
 * @UpdateRemark:
 * @Version: 1.0.0
 */
class SplashBannerAdapter : BaseBannerAdapter<String, SplashBannerViewHolder>() {

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.banner_itemwelcome
    }

    override fun createViewHolder(
        parent: ViewGroup,
        itemView: View,
        viewType: Int
    ): SplashBannerViewHolder {
        return SplashBannerViewHolder(itemView);
    }

    override fun onBind(
        holder: SplashBannerViewHolder?,
        data: String?,
        position: Int,
        pageSize: Int
    ) {
        holder?.bindData(data, position, pageSize);
    }


}
