package com.zzk.jetpackmvvm.ui.pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import kotlinx.android.synthetic.main.fragment_pager.*
import com.zzk.jetpackmvvm.R
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.base.JPBaseFragment
import com.zzk.jetpackmvvm.databinding.FragmentPagerBinding
import com.zzk.jetpackmvvm.ui.me.collect.CollectFragment
import com.zzk.jetpackmvvm.ui.main.search.SearchFragment
import com.zzk.jetpackmvvm.ui.share.AriticleFragment
import com.zzk.jetpackmvvm.ui.me.todo.TodoListFragment

/**
 * @author : hgj
 * @date   : 2020/6/15
 * 测试 Viewpager下的懒加载
 */
class PagerFragment : JPBaseFragment<BaseViewModel, FragmentPagerBinding>() {

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_pager
    }

    override fun initData(savedInstanceState: Bundle?) {
        pagerViewpager.adapter = object :
            FragmentStatePagerAdapter(childFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
            override fun getItem(position: Int): Fragment {
                return when (position) {
                    0 -> {
                        SearchFragment()
                    }
                    1 -> {
                        TodoListFragment()
                    }
                    2 -> {
                        AriticleFragment()
                    }
                    else -> {
                        CollectFragment()
                    }
                }
            }

            override fun getCount(): Int {
                return 5;
            }
        }
        pagerViewpager.offscreenPageLimit = 5
    }

}