package com.zzk.jetpackmvvm.ui.me.demo.lazyload.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.blankj.utilcode.util.LogUtils
import com.google.android.material.tabs.TabLayout
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.databinding.FragmentCBinding
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.adapter.FragmentLazyStatePageAdapter
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generateTextFragmentTitles
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.generateTextFragments
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jpmvvmbase.base.LazyBaseFragment

/**
 * Author:  andy.xwt
 * Date:    2020-01-14 18:33
 * Description:
 */
class CFragment : LazyBaseFragment<BaseViewModel, FragmentCBinding>() {

    //##########################  custom variables start ##########################################

    companion object {
        fun newInstance() = CFragment()
    }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_c
    }

    override fun initData(savedInstanceState: Bundle?) {
        super.initData(savedInstanceState)
        LogUtils.i("CFragment->initData")
    }

    override fun lazyLoadData() {
        LogUtils.i("CFragment->lazyLoadData")
        view?.let {
            val viewPager = it.findViewById<ViewPager>(R.id.view_pager).apply {
                adapter = FragmentLazyStatePageAdapter(
                    childFragmentManager,
                    generateTextFragments(4),
                    generateTextFragmentTitles(4)
                )
            }
            it.findViewById<TabLayout>(R.id.tab_layout)?.setupWithViewPager(viewPager)
        }
    }

    override fun showLoadingDialog(message: String) {
    }

    override fun dismissLoadingDialog() {
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}
