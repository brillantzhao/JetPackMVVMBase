package com.zzk.jetpackmvvm.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.zhpan.bannerview.BannerViewPager
import com.zzk.jetpackmvvm.R
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.base.JPBaseActivity
import com.zzk.jetpackmvvm.databinding.ActivitySplashBinding
import com.zzk.jpmvvmbase.ext.view.gone
import com.zzk.jpmvvmbase.ext.view.visible
import com.zzk.jetpackmvvm.ui.home.HomeActivity
import com.zzk.jetpackmvvm.util.CacheUtil
import com.zzk.jetpackmvvm.util.SettingUtil
import kotlinx.android.synthetic.main.activity_splash.*

/**
 *
 * @ProjectName:    JetPackMVVMBase
 * @Package:        com.zzk.jpmvvmbase.ui.splash
 * @ClassName:      SplashActivity
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 15:59
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 15:59
 * @UpdateRemark:
 * @Version:        1.0.0
 */
@Suppress("DEPRECATED_IDENTITY_EQUALS")
class SplashActivity : JPBaseActivity<BaseViewModel, ActivitySplashBinding>() {

    //##########################  custom variables start ##########################################

    private var resList = arrayOf("唱", "跳", "r a p")

    private lateinit var mViewPager: BannerViewPager<String, SplashBannerViewHolder>

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(savedInstanceState: Bundle?): Int {
        return R.layout.activity_splash
    }

    override fun initData(savedInstanceState: Bundle?) {
        // 防止出现按Home键回到桌面时，再次点击重新进入该界面bug
        if (intent.flags and Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT !== 0) {
            finish()
            return
        }
        mDatabind.click = ProxyClick()
        welcome_baseview.setBackgroundColor(SettingUtil.getColor(this))
        mViewPager = findViewById(R.id.banner_view)
        if (CacheUtil.isFirst() == true) {
            //是第一次打开App 显示引导页
            welcome_image.gone()
            mViewPager.apply {
                adapter = SplashBannerAdapter()
                setLifecycleRegistry(lifecycle)
                registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        super.onPageSelected(position)
                        if (position == resList.size - 1) {
                            welcomeJoin.visible()
                        } else {
                            welcomeJoin.gone()
                        }
                    }
                })
                create(resList.toList())
            }
        } else {
            //不是第一次打开App 0.3秒后自动跳转到主页
            welcome_image.visible()
            mViewPager.postDelayed({
                startActivity(Intent(this, HomeActivity::class.java))
                finish()
                //带点渐变动画
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            }, 300)
        }
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    inner class ProxyClick {
        fun toMain() {
            CacheUtil.setFirst(false)
            startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
            finish()
            // 带点渐变动画
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    //##########################  custom metohds end   ############################################

}