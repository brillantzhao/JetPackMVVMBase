package com.zzk.jetpackmvvm.ui.me.demo.lazyload.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.ui.me.demo.lazyload.fragment.FourFragment
import com.zzk.jpmvvmbase.ext.loadRootFragment

/**
 * Author:  andy.xwt
 * Date:    2020-01-14 14:48
 * Description:
 */
class Activity4 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_4)
        initView()
    }

    private fun initView() {
        loadRootFragment(R.id.fl_container, FourFragment.newInstance())
    }
}