package com.zzk.jetpackmvvm.ui.test

import android.os.Bundle
import androidx.activity.viewModels
import com.blankj.utilcode.util.LogUtils
import com.zzk.jetpackmvvm.R
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.base.JPBaseActivity
import com.zzk.jetpackmvvm.databinding.ActivityTestBinding
import com.zzk.jetpackmvvm.requestviewmodel.RequestLoginRegisterViewModel

/**
 *
 * @ProjectName:    JetPackMVVMBase
 * @Package:        com.zzk.jpmvvmbase.ui.test
 * @ClassName:      TestActivity
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.20 8:59
 * @UpdateUser:
 * @UpdateDate:     2021.1.20 8:59
 * @UpdateRemark:
 * @Version:        1.0.0
 */
class TestActivity : JPBaseActivity<BaseViewModel, ActivityTestBinding>() {

    //##########################  custom variables start ##########################################

    val viewModel: RequestLoginRegisterViewModel by viewModels()

    val adapter: TestAdapter by lazy { TestAdapter(arrayListOf()) }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(savedInstanceState: Bundle?): Int {
        return R.layout.activity_test
    }

    override fun initData(savedInstanceState: Bundle?) {
        //强烈注意：使用addLoadingObserve 将非绑定当前activity的viewmodel绑定loading回调 防止出现请求时不显示 loading 弹窗bug
        addLoadingObserve(viewModel)

        adapter.run {
            clickAction = { position, item, state ->
                LogUtils.d("海王收到了点击事件，并准备发一个红包")
            }
        }
    }

    override fun initViewObservable() {
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}