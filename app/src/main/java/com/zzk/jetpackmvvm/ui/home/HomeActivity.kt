package com.zzk.jetpackmvvm.ui.home

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.blankj.utilcode.util.ToastUtils
import com.tencent.bugly.beta.Beta
import com.zzk.jetpackmvvm.R
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.base.JPBaseActivity
import com.zzk.jetpackmvvm.databinding.ActivityHomeBinding
import com.zzk.jetpackmvvm.util.StatusBarUtil
import com.zzk.jpmvvmbase.network.manager.NetState

/**
 *
 * @ProjectName:    JetPackMVVMBase
 * @Package:        com.zzk.jpmvvmbase.ui.home
 * @ClassName:      HomeActivity
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 16:58
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 16:58
 * @UpdateRemark:
 * @Version:        1.0.0
 */
class HomeActivity : JPBaseActivity<BaseViewModel, ActivityHomeBinding>() {

    //##########################  custom variables start ##########################################

    var exitTime = 0L

    //##########################  custom variables end  ###########################################

    override fun initContentView(savedInstanceState: Bundle?): Int {
        return R.layout.activity_home
    }

    override fun initData(savedInstanceState: Bundle?) {
        //进入首页检查更新
        Beta.checkUpgrade(false, true)

        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val nav = Navigation.findNavController(this@HomeActivity, R.id.host_fragment)
                if (nav.currentDestination != null && nav.currentDestination!!.id != R.id.homefragment) {
                    //如果当前界面不是主页，那么直接调用返回即可
                    nav.navigateUp()
                } else {
                    //是主页
                    if (System.currentTimeMillis() - exitTime > 2000) {
                        ToastUtils.showShort("再按一次退出程序")
                        exitTime = System.currentTimeMillis()
                    } else {
                        finish()
                    }
                }
            }
        })
        appViewModel.appColor.value?.let {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
            supportActionBar?.setBackgroundDrawable(ColorDrawable(it))
            StatusBarUtil.setColor(this, it, 0)
        }
    }

    override fun initViewObservable() {
        appViewModel.appColor.observe(this, Observer {
            supportActionBar?.setBackgroundDrawable(ColorDrawable(it))
            StatusBarUtil.setColor(this, it, 0)
        })
    }

    /**
     * 示例，在Activity/Fragment中如果想监听网络变化，可重写onNetworkStateChanged该方法
     */
    override fun onNetworkStateChanged(netState: NetState) {
        super.onNetworkStateChanged(netState)
        if (netState.isSuccess) {
            Toast.makeText(applicationContext, "我特么终于有网了啊!", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(applicationContext, "我特么怎么断网了!", Toast.LENGTH_SHORT).show()
        }
    }

    //##########################  override custom metohds start ###################################

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}