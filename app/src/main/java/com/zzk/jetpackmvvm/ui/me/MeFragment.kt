package com.zzk.jetpackmvvm.ui.me

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.blankj.utilcode.util.ToastUtils
import com.zzk.jpmvvmbase.ext.navController
import com.zzk.jpmvvmbase.ext.navigateAction
import com.zzk.jpmvvmbase.ext.parseState
import com.zzk.jpmvvmbase.ext.util.notNull
import com.zzk.jetpackmvvm.R
import com.zzk.jetpackmvvm.base.JPBaseFragment
import com.zzk.jetpackmvvm.data.model.bean.BannerResponse
import com.zzk.jetpackmvvm.data.model.bean.IntegralResponse
import com.zzk.jetpackmvvm.databinding.FragmentMeBinding
import com.zzk.jetpackmvvm.ext.*
import com.zzk.jetpackmvvm.requestviewmodel.RequestMeViewModel
import kotlinx.android.synthetic.main.fragment_me.*

/**
 *
 * @ProjectName:
 * @Package:        com.zzk.jetpackmvvm
 * @ClassName:
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 15:50
 * @UpdateRemark:
 * @Version:        1.0.0
 */
class MeFragment : JPBaseFragment<MeViewModel, FragmentMeBinding>() {

    //##########################  custom variables start ##########################################

    // 积分排名信息
    private var rank: IntegralResponse? = null

    //
    private val requestMeViewModel: RequestMeViewModel by viewModels()

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun initContentView(
        inflater: LayoutInflater?,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Int {
        return R.layout.fragment_me
    }

    override fun initData(savedInstanceState: Bundle?) {
        // viewModel
        mDatabind.viewModel = mViewModel
        // proxyClick
        mDatabind.proxyClick = ProxyClick()
        // 全局的信息
        appViewModel.appColor.value?.let { setUiTheme(it, me_linear, me_integral) }
        appViewModel.userinfo.value?.let { mViewModel.name.set(if (it.nickname.isEmpty()) it.username else it.nickname) }
        //
        me_swipe.init {
            requestMeViewModel.getIntegral()
        }
    }

    override fun lazyLoadData() {
        appViewModel.userinfo.value?.run {
            me_swipe.isRefreshing = true
            requestMeViewModel.getIntegral()
        }
    }

    override fun initViewObservable() {
        requestMeViewModel.meData.observe(viewLifecycleOwner, Observer { resultState ->
            me_swipe.isRefreshing = false
            parseState(resultState, {
                rank = it
                mViewModel.info.set("id：${it.userId}　排名：${it.rank}")
                mViewModel.integral.set(it.coinCount)
            }, {
                ToastUtils.showShort(it.errorMsg)
            })
        })

        appViewModel.run {
            appColor.observe(viewLifecycleOwner, Observer {
                setUiTheme(it, me_linear, me_swipe, me_integral)
            })
            userinfo.observe(viewLifecycleOwner, Observer {
                it.notNull({
                    me_swipe.isRefreshing = true
                    mViewModel.name.set(if (it.nickname.isEmpty()) it.username else it.nickname)
                    requestMeViewModel.getIntegral()
                }, {
                    mViewModel.name.set("请先登录~")
                    mViewModel.info.set("id：--　排名：--")
                    mViewModel.integral.set(0)
                })
            })
        }
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    inner class ProxyClick {

        /** 登录 */
        fun login() {
            navController().jumpByLogin {}
        }

        /** 积分 */
        fun integral() {
            navController().jumpByLogin {
                it.navigateAction(R.id.action_mainfragment_to_integralFragment,
                    Bundle().apply {
                        putParcelable("rank", rank)
                    }
                )
            }
        }

        /** 收藏 */
        fun collect() {
            navController().jumpByLogin {
                it.navigateAction(R.id.action_mainfragment_to_collectFragment)
            }
        }

        /** 文章 */
        fun ariticle() {
            navController().jumpByLogin {
                it.navigateAction(R.id.action_mainfragment_to_ariticleFragment)
            }
        }

        /**  */
        fun todo() {
            navController().jumpByLogin {
                it.navigateAction(R.id.action_mainfragment_to_todoListFragment)
            }
        }

        /** 玩Android开源网站 */
        fun about() {
            navController().navigateAction(R.id.action_to_webFragment, Bundle().apply {
                putParcelable(
                    "bannerdata",
                    BannerResponse(
                        title = "玩Android网站",
                        url = "https://www.wanandroid.com/"
                    )
                )
            })
        }

        /** 加入我们 */
        fun join() {
            joinQQGroup("9n4i5sHt4189d4DvbotKiCHy-5jZtD4D")
        }

        /**demo*/
        fun demo() {
            navController().navigateAction(R.id.action_mainfragment_to_demoFragment)
        }

        /** 设置*/
        fun setting() {
            navController().navigateAction(R.id.action_mainfragment_to_settingFragment)
        }

    }

    //##########################  custom metohds end   ############################################

}