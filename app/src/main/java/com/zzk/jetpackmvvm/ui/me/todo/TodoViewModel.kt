package com.zzk.jetpackmvvm.ui.me.todo

import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jpmvvmbase.callback.databind.IntObservableField
import com.zzk.jpmvvmbase.callback.databind.StringObservableField
import com.zzk.jetpackmvvm.data.model.enums.TodoType
import com.zzk.jpmvvmbase.ext.launch

/**
 * 作者　: hegaojian
 * 时间　: 2020/3/11
 * 描述　:
 */
class TodoViewModel : BaseViewModel() {

    //标题
    var todoTitle = StringObservableField()

    //内容
    var todoContent =
        StringObservableField()

    //时间
    var todoTime = StringObservableField()

    //优先级
    var todoLeve =
        StringObservableField(TodoType.TodoType1.content)

    //优先级颜色
    var todoColor =
        IntObservableField(TodoType.TodoType1.color)

    fun xx(): Unit {
        launch({

        }, {

        })
    }
}