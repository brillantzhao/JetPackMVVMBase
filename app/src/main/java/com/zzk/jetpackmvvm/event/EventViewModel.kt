package com.zzk.jetpackmvvm.event

import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jpmvvmbase.callback.livedata.event.EventLiveData
import com.zzk.jetpackmvvm.data.model.bean.CollectBus

/**
 * APP全局的ViewModel，可以在这里发送全局通知替代EventBus，LiveDataBus等
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 15:50
 * @UpdateRemark:
 * @Version: 1.0.0
 */
class EventViewModel : BaseViewModel() {

    //全局收藏，在任意一个地方收藏或取消收藏，监听该值的界面都会收到消息
    val collectEvent = EventLiveData<CollectBus>()

    //分享文章通知
    val shareArticleEvent = EventLiveData<Boolean>()

    //添加TODO通知
    val todoEvent = EventLiveData<Boolean>()

}