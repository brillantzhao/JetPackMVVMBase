package com.zzk.jetpackmvvm.requestviewmodel

import androidx.lifecycle.MutableLiveData
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.data.model.bean.IntegralResponse
import com.zzk.jpmvvmbase.ext.request
import com.zzk.jetpackmvvm.network.apiService
import com.zzk.jpmvvmbase.state.ResultState

class RequestMeViewModel : BaseViewModel() {

    var meData = MutableLiveData<ResultState<IntegralResponse>>()

    fun getIntegral() {
        request({ apiService.getIntegral() }, meData)
    }
}