package com.zzk.jetpackmvvm.requestviewmodel

import androidx.lifecycle.MutableLiveData
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.data.model.bean.*
import com.zzk.jpmvvmbase.ext.request
import com.zzk.jetpackmvvm.network.apiService
import com.zzk.jetpackmvvm.network.stateCallback.ListDataUiState
import com.zzk.jpmvvmbase.state.ResultState

/**
 * 作者　: hegaojian
 * 时间　: 2020/2/29
 * 描述　:
 */
class RequestPublicNumberViewModel : BaseViewModel() {

    var pageNo = 1

    var titleData: MutableLiveData<ResultState<ArrayList<ClassifyResponse>>> = MutableLiveData()

    var publicDataState: MutableLiveData<ListDataUiState<AriticleResponse>> = MutableLiveData()

    fun getPublicTitleData() {
        request({ apiService.getPublicTitle() }, titleData)
    }

    fun getPublicData(isRefresh: Boolean, cid: Int) {
        if (isRefresh) {
            pageNo = 1
        }
        request({ apiService.getPublicData(pageNo, cid) }, {
            //请求成功
            pageNo++
            val listDataUiState =
                ListDataUiState(
                    isSuccess = true,
                    isRefresh = isRefresh,
                    isEmpty = it.isEmpty(),
                    hasMore = it.hasMore(),
                    isFirstEmpty = isRefresh && it.isEmpty(),
                    listData = it.datas
                )
            publicDataState.value = listDataUiState
        }, {
            //请求失败
            val listDataUiState =
                ListDataUiState(
                    isSuccess = false,
                    errMessage = it.errorMsg,
                    isRefresh = isRefresh,
                    listData = arrayListOf<AriticleResponse>()
                )
            publicDataState.value = listDataUiState
        })
    }
}