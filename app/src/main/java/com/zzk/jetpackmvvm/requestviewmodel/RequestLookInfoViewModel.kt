package com.zzk.jetpackmvvm.requestviewmodel

import androidx.lifecycle.MutableLiveData
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.data.model.bean.*
import com.zzk.jpmvvmbase.ext.request
import com.zzk.jetpackmvvm.network.apiService
import com.zzk.jetpackmvvm.network.stateCallback.ListDataUiState

/**
 * 作者　: hegaojian
 * 时间　: 2020/3/4
 * 描述　:
 */
class RequestLookInfoViewModel : BaseViewModel() {

    var pageNo = 1

    var shareListDataUistate = MutableLiveData<ListDataUiState<AriticleResponse>>()

    var shareResponse = MutableLiveData<ShareResponse>()

    fun getLookinfo(id: Int, isRefresh: Boolean) {
        if (isRefresh) {
            pageNo = 1
        }
        request({ apiService.getShareByidData(id, pageNo) }, {
            //请求成功
            pageNo++
            shareResponse.value = it
            val listDataUiState =
                ListDataUiState(
                    isSuccess = true,
                    isRefresh = it.shareArticles.isRefresh(),
                    isEmpty = it.shareArticles.isEmpty(),
                    hasMore = it.shareArticles.hasMore(),
                    isFirstEmpty = isRefresh && it.shareArticles.isEmpty(),
                    listData = it.shareArticles.datas
                )
            shareListDataUistate.value = listDataUiState
        }, {
            //请求失败
            val listDataUiState =
                ListDataUiState(
                    isSuccess = false,
                    errMessage = it.errorMsg,
                    isRefresh = isRefresh,
                    listData = arrayListOf<AriticleResponse>()
                )
            shareListDataUistate.value = listDataUiState
        })
    }
}