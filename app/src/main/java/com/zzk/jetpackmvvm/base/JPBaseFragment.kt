package com.zzk.jetpackmvvm.base

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.zzk.jpmvvmbase.base.BaseFragment
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.event.AppViewModel
import com.zzk.jetpackmvvm.event.EventViewModel
import com.zzk.jpmvvmbase.ext.getAppViewModel
import com.zzk.jetpackmvvm.ext.dismissLoadingExt
import com.zzk.jetpackmvvm.ext.hideSoftKeyboard
import com.zzk.jetpackmvvm.ext.showLoadingExt
import com.zzk.jpmvvmbase.network.manager.NetState
import com.zzk.jpmvvmbase.network.manager.NetworkStateManager

/**
 * 项目中的Fragment基类，在这里实现显示弹窗，吐司，还有自己的需求操作
 *
 * @ProjectName:    JetPackMVVMBase
 * @Package:        com.zzk.jpmvvmbase.base
 * @ClassName:      MyBaseFragment
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 16:16
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 16:16
 * @UpdateRemark:
 * @Version:        1.0.0
 */
abstract class JPBaseFragment<VM : BaseViewModel, DB : ViewDataBinding> : BaseFragment<VM, DB>() {

    //##########################  custom variables start ##########################################

    //是否第一次加载
    private var isFirst: Boolean = true

    //Application全局的ViewModel，里面存放了一些账户信息，基本配置信息等
    val appViewModel: AppViewModel by lazy { getAppViewModel<AppViewModel>() }

    //Application全局的ViewModel，用于发送全局通知操作
    val eventViewModel: EventViewModel by lazy { getAppViewModel<EventViewModel>() }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    /**
     * 打开等待框
     */
    override fun showLoadingDialog(message: String) {
        showLoadingExt(message)
    }

    /**
     * 关闭等待框
     */
    override fun dismissLoadingDialog() {
        dismissLoadingExt()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isFirst = true
    }

    override fun onResume() {
        super.onResume()
        onVisible()
    }

    override fun onPause() {
        super.onPause()
        hideSoftKeyboard(activity)
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    /**
     * 非抽象方法，网络变化监听，子类按需要重写
     */
    open fun onNetworkStateChanged(netState: NetState) {}

    /**
     * 非抽象方法，懒加载，子类按需要重写
     */
    open fun lazyLoadData() {}

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    /**
     * 处理懒加载的一种方式
     * 同时处理网络监听
     */
    private fun onVisible() {
        if (lifecycle.currentState == Lifecycle.State.STARTED && isFirst) {
            //等待view加载后触发懒加载
            view?.post {
                lazyLoadData()
                //在Fragment中，只有懒加载过了才能开启网络变化监听
                NetworkStateManager.instance.mNetworkStateCallback.observeInFragment(
                    this,
                    Observer {
                        //不是首次订阅时调用方法，防止数据第一次监听错误
                        if (!isFirst) {
                            onNetworkStateChanged(it)
                        }
                    })
                isFirst = false
            }
        }
    }

    //##########################  custom metohds end   ############################################

}