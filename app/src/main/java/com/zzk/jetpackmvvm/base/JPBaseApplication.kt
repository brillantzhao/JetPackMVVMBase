package com.zzk.jetpackmvvm.base

import com.tencent.mmkv.MMKV
import com.zzk.jetpackmvvm.R
import com.zzk.jpmvvmbase.base.BaseApplication

/**
 * 添加该类的目的是在最精简BaseApplication的基础上封装一些和应用相关的初始化工作，最主要是满足组件化的需求，
 * 如果没有组件化的需要的话，可以去掉该层封装，直接放在最终的MyApplication中
 *
 * @ProjectName:    JetPackMVVMBase
 * @Package:        com.zzk.jetpackmvvm.base
 * @ClassName:      JPBaseApplication
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.21 9:45
 * @UpdateUser:
 * @UpdateDate:     2021.1.21 9:45
 * @UpdateRemark:
 * @Version:        1.0.0
 */
open class JPBaseApplication : BaseApplication() {

    //##########################  custom variables start ##########################################

    var isOnlineVersion = false
    var isShowLog = false

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun onCreate() {
        super.onCreate()
        //
        initGlobalInfo()
        // 调用父类的初始化方法
        initUtilCodeXLog(isShowLog)
        // 初始化 MMKV，设定 MMKV 的根目录（files/mmkv/）
        MMKV.initialize(this.filesDir.absolutePath + "/mmkv")
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    private fun initGlobalInfo() {
        // 获取app当前是否是ONLINE状态，即正式的线上发布状态，此时需要关闭日志
        isOnlineVersion = resources.getBoolean(R.bool.isOnlineVersion)
        // 判断日志是否展示
        isShowLog = resources.getBoolean(R.bool.isShowLog)
    }

    //##########################  custom metohds end   ############################################

}