package com.zzk.jetpackmvvm.base

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.zzk.jpmvvmbase.base.BaseActivity
import com.zzk.jpmvvmbase.base.BaseViewModel
import com.zzk.jetpackmvvm.event.AppViewModel
import com.zzk.jetpackmvvm.event.EventViewModel
import com.zzk.jpmvvmbase.ext.getAppViewModel
import com.zzk.jetpackmvvm.ext.dismissLoadingExt
import com.zzk.jetpackmvvm.ext.showLoadingExt
import com.zzk.jpmvvmbase.network.manager.NetState
import com.zzk.jpmvvmbase.network.manager.NetworkStateManager

/**
 * 项目中的Activity基类，在这里实现显示弹窗，吐司，还有加入自己的需求操作
 *
 * @ProjectName:    JetPackMVVMBase
 * @Package:        com.zzk.jpmvvmbase.base
 * @ClassName:      MyBaseActivity
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.19 16:16
 * @UpdateUser:
 * @UpdateDate:     2021.1.19 16:16
 * @UpdateRemark:
 * @Version:        1.0.0
 */
abstract class JPBaseActivity<VM : BaseViewModel, DB : ViewDataBinding> : BaseActivity<VM, DB>() {

    //##########################  custom variables start ##########################################

    //Application全局的ViewModel，里面存放了一些账户信息，基本配置信息等
    val appViewModel: AppViewModel by lazy { getAppViewModel<AppViewModel>() }

    //Application全局的ViewModel，用于发送全局通知操作
    val eventViewModel: EventViewModel by lazy { getAppViewModel<EventViewModel>() }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 开启网络变化监听
        NetworkStateManager.instance.mNetworkStateCallback.observeInActivity(this, Observer {
            onNetworkStateChanged(it)
        })
    }

    /**
     * 打开等待框
     */
    override fun showLoadingDialog(message: String) {
        showLoadingExt(message)
    }

    /**
     * 关闭等待框
     */
    override fun dismissLoadingDialog() {
        dismissLoadingExt()
    }

    /* */
    /**
     * 在任何情况下本来适配正常的布局突然出现适配失效，适配异常等问题，只要重写 Activity 的 getResources() 方法
     *//*
    override fun getResources(): Resources {
        AutoSizeCompat.autoConvertDensityOfGlobal(super.getResources())
        return super.getResources()
    }*/

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    /**
     * 网络变化监听 子类重写
     */
    open fun onNetworkStateChanged(netState: NetState) {}

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}