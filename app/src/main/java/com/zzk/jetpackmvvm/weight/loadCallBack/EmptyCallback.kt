package com.zzk.jetpackmvvm.weight.loadCallBack

import com.kingja.loadsir.callback.Callback
import com.zzk.jetpackmvvm.R

/**
 *
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 15:50
 * @UpdateRemark:
 * @Version: 1.0.0
 */
class EmptyCallback : Callback() {

    override fun onCreateView(): Int {
        return R.layout.layout_empty
    }

}