package com.zzk.jetpackmvvm.weight.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View

/**
 *
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 15:50
 * @UpdateRemark:
 * @Version: 1.0.0
 */
class MyView(context: Context) : View(context) {

    constructor(context: Context, attr: AttributeSet) : this(context) {

    }
}