package com.zzk.jetpackmvvm.network.stateCallback

/**
 * 收藏数据状态类
 * @ProjectName: JetPackMVVMBase
 * @Package: com.zzk.jpmvvmbase
 * @ClassName:
 * @Description:
 * @Author: brilliantzhao
 * @CreateDate: 2021.1.19 15:50
 * @UpdateUser:
 * @UpdateDate: 2021.1.19 15:50
 * @UpdateRemark:
 * @Version: 1.0.0
 */
data class CollectUiState(
    //请求是否成功
    var isSuccess: Boolean = true,
    //收藏
    var collect: Boolean = false,
    //收藏Id
    var id: Int = -1,
    //请求失败的错误信息
    var errorMsg: String = ""
)